//API carrusel productos recomendados
let url = "http://127.0.0.1:8887/infoJuanValdez.json";

let contenedorProductosRecomendados = document.getElementById("contenedorProductosRecomendados");

fetch(url)

//La respuesta es exitosa
.then((resp)=>resp.json())
.then(function(data){
  console.log(data);
  
  for(let m = 0; m < data.length; m++){
    console.log(data[m])
    let wrapperProducto3 = document.createElement("div");
    wrapperProducto3.classList.add("swiper-slide");
    wrapperProducto3.classList.add("swiper-slide3");

    let nombreProducto = data[m].productName;
    let precioProducto = data[m].items[0].sellers[0].commertialOffer.Price;
    let imagenProducto = data[m].items[0].images[0].imageUrl;
    let marcaProducto = data[m].brand;

    let markupHTMLProducto = `
    <div class="productohome">
    <div class="fotoproducto">
      <img src=${imagenProducto} alt="">
      <button class="avisoahorro">Ahorra 30%</button>
      <span class="corazon corazonproducto"></span>
      <div class="vistapreviaback">
        <p>Vista rápida</p>
      </div>
    </div>
    <p class="nombreproducto">${nombreProducto}</p>
    <p class="textocategoria">Categoría</p>
    <p class="precioproducto"><span class="precioanterior">${precioProducto}</span> $17.700</p>
    <a href="compraProductos.html"><button class="productohome-boton">Agregar al carrito</button></a>
  </div>`;
  wrapperProducto3.innerHTML = markupHTMLProducto;
  contenedorProductosRecomendados.appendChild(wrapperProducto3);
  }
})

//La respuesta es negativa
.catch(function(error){
  console.log(error);
})


//API Carrusel nuevos lanzamientos
let contenedorNuevosLanzamientos = document.getElementById("contenedorNuevosLanzamientos");

fetch(url)

//La respuesta es exitosa
.then((resp)=>resp.json())
.then(function(data){
  
  for(let m = 0; m < data.length; m++){
   
    let wrapperProducto4 = document.createElement("div");
    wrapperProducto4.classList.add("swiper-slide");
    wrapperProducto4.classList.add("swiper-slide4");

    let nombreProducto = data[m].productName;
    let precioProducto = data[m].items[0].sellers[0].commertialOffer.Price;
    let imagenProducto = data[m].items[0].images[0].imageUrl;
    let marcaProducto = data[m].brand;

    let markupHTMLProducto = `
    <div class="productohome">
    <div class="fotoproducto">
      <img src=${imagenProducto} alt="">
      <button class="avisoahorro">Ahorra 30%</button>
      <span class="corazon corazonproducto"></span>
      <div class="vistapreviaback">
        <p>Vista rápida</p>
      </div>
    </div>
    <p class="nombreproducto">${nombreProducto}</p>
    <p class="textocategoria">Categoría</p>
    <p class="precioproducto"><span class="precioanterior">${precioProducto}</span> $17.700</p>
    <a href="compraProductos.html"><button class="productohome-boton">Agregar al carrito</button></a>
  </div>`;
  wrapperProducto4.innerHTML = markupHTMLProducto;
  contenedorNuevosLanzamientos.appendChild(wrapperProducto4);
  }
})


//La respuesta es negativa
.catch(function(error){
  console.log(error);
})

