//Carrusel principal
var swiper = new Swiper(".mySwiper1", {
  slidesPerView: 1,
  spaceBetween: 5,
  slidesPerGroup: 4,
  pagination: {
    el: ".swiper-pagination1",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next1",
    prevEl: ".swiper-button-prev1",
  },
});

//Carrusel Banner1 mobile
var swiper = new Swiper(".mySwiperBanner", {
  slidesPerView: "auto",
  centeredSlides: true,
  spaceBetween: 30,
  pagination: {
    el: ".swiper-pagination-banner1",
    clickable: true,
  },
});

//Carrusel categorías
var swiper = new Swiper(".mySwiper2", {
  slidesPerView: 1,
  spaceBetween: 5,
  slidesPerGroup: 3,
  pagination: {
    el: ".swiper-pagination2",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next2",
    prevEl: ".swiper-button-prev2",
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 10,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 5,
      spaceBetween: 25,
    },
  },
});

//Carrusel categorías mobile
var swiper = new Swiper(".mySwiperCarruselCategorias", {
  slidesPerView: "auto",
  slidesPerGroup: 1,
  loop: true,
  centeredSlides: true,
  spaceBetween: 30,
  pagination: {
    el: ".swiper-pagination-carruselcategorias",
    clickable: true,
  },
});


//Carrusel productos recomendados
setTimeout(function() {
var swiper = new Swiper(".mySwiper3", {
  slidesPerView: 2,
  spaceBetween: 5,
  slidesPerGroup: 4,
  pagination: {
    el: ".swiper-pagination3",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next3",
    prevEl: ".swiper-button-prev3",
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 10,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 25,
    },
  },
});
}, 1050)

//Carrusel nuevos lanzamientos
setTimeout(function() {
var swiper = new Swiper(".mySwiper4", {
  slidesPerView: 2,
  spaceBetween: 5,
  slidesPerGroup: 4,
  pagination: {
    el: ".swiper-pagination4",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next4",
    prevEl: ".swiper-button-prev4",
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 10,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 25,
    },
  },
});
}, 1050)